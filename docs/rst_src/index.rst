.. _doc:

.. Note::  This documentation is being updated with material from the
   Fuego wiki (at fuegotest.org).  Please be patient while this work is
   in progress.

.. include:: FrontPage.rst

################
Index
################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction
   Architecture

.. _tutor:

.. toctree::
   :maxdepth: 2
   :caption: Tutorials:

   Install_and_First_Test

.. _admin:

.. toctree::
   :maxdepth: 2
   :caption: Installation and Administration:

   Quickstart_Guide
   Installing_Fuego
   Adding_a_Board
   Adding_a_toolchain
   Adding_test_jobs_to_Jenkins
   Adding_views_to_Jenkins
   Test_variables

.. _user_guides:

.. toctree::
   :maxdepth: 2
   :caption: User Guides:

   Jenkins_User_Interface

.. toctree::
   :maxdepth: 2
   :caption: Advanced Topics:

   Adding_or_Customizing_a_Distribution
   integration_with_ttc
   Working_with_remote_boards

.. _dev_res:

.. toctree::
   :maxdepth: 2
   :caption: Developer resources

   Building_Documentation
   Fuego_Developer_Notes
   License_And_Contribution_Policy

.. _api_rex:

.. toctree::
   :maxdepth: 2
   :caption: API Reference

   Core_interfaces
   Adding_a_new_test
   Using_Batch_tests
   Parser_module_API


.. toctree::
   :maxdepth: 2
   :caption: Reference Material:

   devref
   FAQ
   Other_Test_Systems
   Fuego_naming_rules
   Artwork
   Glossary

.. toctree::
   :hidden:

   Raspberry_Pi_Fuego_Setup
   Using_the_qemuarm_target

.. toctree::
   :hidden:

   Fuego_Outline

.. toctree::
   :hidden:

   Variables
   Dynamic_Variables
   Metrics

.. toctree::
   :hidden:

   funtionalltp
   Troubleshooting_Guide
   Presentations
   OSS_Test_Vision
   Test_Specs_and_Plans

.. toctree::
   :hidden:

   Help
   Function_Unpack

.. toctree::
   :hidden:

   function_test_pre_check
   function_test_build
   function_test_deploy
   function_test_run
   function_test_processing

.. toctree::
   :hidden:

   function_log_compare


.. toctree::
   :hidden:

   parser.py
   criteria.json
   reference.json
   Coding_style.rst


.. toctree::
   :hidden:

   Testplan_Reference
   function_run_test
   function_allocate_next_batch_id

.. toctree::
   :hidden:

   parser_func_parse_log
   parser_func_process
   parser_func_split_output_per_testcase



===================
Indices and tables
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


..
   The following is to hide a warning.
   FrontPage.rst is included rather than referenced using toctree

.. toctree::
  :hidden:

  FrontPage
  Sandbox
  Sandbox2
